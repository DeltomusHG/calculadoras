
console.log("script en ejecucion")
const btn = document.getElementById("submit_button")
let id2 = new URL(window.location.href).searchParams.get("id")
console.log(id2)
btn.addEventListener("click", function(){
    let menu = document.getElementById("tarjetadecontrol").value
    let id = new URL(window.location.href).searchParams.get("id")
    console.log("Variable menu"+menu)
    console.log("id de paciente:"+id)
    if (menu == "Control de enfermedades crónicas") {
        cec(id)
    } else if (menu == "Control de la mujer embarazada y lactancia"){
        cmeyl(id)
    } else if (menu == "Control de nutrición de la niña y el niño") {
        control_infantil(id)
    }

})

function cec(id){
    console.log(id)
    let peso = document.getElementById("peso").value
    let imc = document.getElementById("imc").value
    let cc = document.getElementById("cc").value
    let sistolica = document.getElementById("sistolica").value
    let distolica = document.getElementById("distolica").value
    let glucemia = document.getElementById("glucemia").value
    let hba1c = document.getElementById("hba1c").value
    let pies = document.getElementById("pies").value
    let totalcolesterol = document.getElementById("totalcolesterol").value
    let ldl = document.getElementById("ldl").value
    let hdl = document.getElementById("hdl").value
    let trigliceridos = document.getElementById("trigliceridos").value
    let trat_nofarma = document.getElementById("tratamiento_nofarmacologico").value
    let trat_farmacologico = document.getElementById("farmacologico").value
    let pacientecontrolado = document.getElementById("pacientecontrolado").value
    let gpo_ayuda = document.getElementById("ayudamutua").value
    let complicaciones = document.getElementById("complicaciones").value
    let observaciones = document.getElementById("observaciones").value
    let cartilla = "cec"
    let data_cec = `{
        "id" : "${id}",
        "cartilla" : "${cartilla}",
        "peso": "${peso}",
        "imc" : "${imc}",
        "cc" : "${cc}",
        "sistolica" : "${sistolica}",
        "distolica" : "${distolica}",
        "glucemia" : "${glucemia}",
        "hba1c" : "${hba1c}",
        "pies" : "${pies}",
        "totalcolesterol" : "${totalcolesterol}",
        "ldl" : "${ldl}",
        "hdl" : "${hdl}",
        "trigliceridos" : "${trigliceridos}",
        "trat_nofarma" : "${trat_nofarma}",
        "trat_farmacologico" : "${trat_farmacologico}",
        "pacientecontrolado" : "${pacientecontrolado}",
        "gpo_ayuda": "${gpo_ayuda}",
        "complicaciones" : "${complicaciones}",
        "observaciones" : "${observaciones}"
    }`

    console.log(data_cec)

    $.ajax({
        type: "POST",
        url: "https://herramientas.medikaway.com/cec",
        dataType: "json",
        contentType: 'application/json',
        data: data_cec,        
        success: function(response) {
            swal("¡Listo!", "La cartilla de control se ha almacenado exitosamente.", "success");
            console.log(response)
        },
        error: function(response){
            swal("Algo ha salido mal...", "La cartilla de control no se ha almacenado.", "error");
            console.log(response)
        }
    })

}

function cmeyl(id){
    let gesta = document.getElementById("gesta").value
    let cesareas = document.getElementById("cesareas").value
    let abortos = document.getElementById("abortos").value
    let vivos = document.getElementById("vivos").value
    let muertos = document.getElementById("muertos").value
    let tipoanticonceptivo = document.getElementById("tipoanticonceptivo").value
    let tiempodeuso = document.getElementById("tiempodeuso").value
    let habitotabaquico = document.getElementById("habitotabaquico").value
    let semanasdegestacion = document.getElementById("semanasdegestacion").value
    let pesoembarazada = document.getElementById("pesoembarazada").value
    let tensionarterial = document.getElementById("tensionarterial").value
    let fondouterino = document.getElementById("fondouterino").value
    let signosdealarma = document.getElementById("signosdealarma").value
    let medicamentos = document.getElementById("medicamentos").value
    let analisisclinicos = document.getElementById("analisisclinicos").value
    let examenorina = document.getElementById("examenorina").value
    let acidofolico = document.getElementById("acidofolico").value
    let ayudaalimentos = document.getElementById("ayudaalimentos").value
    let observacionesemb = document.getElementById("observacionesemb").value
    let cartilla = "cmeyl"

    let data_cmeyl = `{
        "id" : "${id}",
        "cartilla" : "${cartilla}",
        "gesta" : "${gesta}",
        "cesareas" : "${cesareas}",
        "abortos" : "${abortos}",
        "vivos" : "${vivos}",
        "muertos" : "${muertos}",
        "tipoanticonceptivo" : "${tipoanticonceptivo}",
        "tiempodeuso" : "${tiempodeuso}",
        "habitotabaquico" : "${habitotabaquico}",
        "semanasdegestacion" : "${semanasdegestacion}",
        "pesoembarazada" : "${pesoembarazada}",
        "tensionarterial" : "${tensionarterial}",
        "fondouterino" : "${fondouterino}",
        "signosdealarma" : "${signosdealarma}",
        "medicamentos" : "${medicamentos}",
        "analisisclinicos" : "${analisisclinicos}",
        "examenorina" : "${examenorina}",
        "acidofolico" : "${acidofolico}",
        "ayudaalimentos" : "${ayudaalimentos}",
        "observacionesemb" : "${observacionesemb}"
    }`

    console.log(data_cmeyl)

    $.ajax({
        type: "POST",
        url: "https://herramientas.medikaway.com/cmeyl",
        dataType: "json",
        contentType: 'application/json',
        data: data_cmeyl,        
        success: function(response) {
            swal("¡Listo!", "La cartilla de control se ha almacenado exitosamente.", "success");
            console.log(response)
        },
        error: function(response){
            swal("Algo ha salido mal...", "La cartilla de control no se ha almacenado.", "error");
            console.log(response)
        }
    })

}

function control_infantil(id){
    let edadmeses = document.getElementById("edadmeses").value
    let resultado = document.getElementById("resultado").value
    let pesonino = document.getElementById("pesonino").value
    let tallanino = document.getElementById("tallanino").value
    let imcnino = document.getElementById("imcnino").value
    let diagnosticonutricional = document.getElementById("diagnosticonutricional").value
    let orientacionalimentaria = document.getElementById("orientacionalimentaria").value
    let observacionesnino = document.getElementById("observacionesnino").value
    
    let cartilla = "control_infantil"
    let data_control_infantil = {
        "id" : id,
        "cartilla" : cartilla,
        "edadmeses" : edadmeses,
        "resultado" : resultado,
        "pesonino" : pesonino,
        "tallanino" : tallanino,
        "imcnino" : imcnino,
        "diagnosticonutricional" : diagnosticonutricional,
        "orientacionalimentaria" : orientacionalimentaria,
        "observacionesnino" : observacionesnino
    }
    let control_form = `{"id":"${id}","cartilla":"${cartilla}","edadmeses":"${edadmeses}","resultado":"${resultado}","pesonino":"${pesonino}","tallanino":"${tallanino}","imcnino":"${imcnino}","diagnosticonutricional":"${diagnosticonutricional}","orientacionalimentaria":"${orientacionalimentaria}","observacionesnino":"${observacionesnino}"}`
    console.log(control_form)
    //data_control_infantil = JSON.parse(control_form)
    console.log(data_control_infantil)

    console.log(data_control_infantil)
    $.ajax({
        type:'POST',
        url: 'https://herramientas.medikaway.com/control_infantil',
        contentType: 'application/json',
        dataType: 'json',
        data : control_form,        
        success: function(response) {
            swal("¡Listo!", "La cartilla de control se ha almacenado exitosamente.", "success");
            console.log(response)
        },
        error: function(response){
            swal("Algo ha salido mal...", "La cartilla de control no se ha almacenado.", "error");
            console.log(response)
        }
    })
}
