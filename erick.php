<?php
$results = array(
	array('name' => 'Anatomía Patológica',
        'r_13' => 63.777,
        'r_14' => 64.666,
        'r_15' => 66.223,
        'r_16' => 68.000,
        'r_17' => 68.667),
	array('name' => 'Anestesiología',
        'r_13' => 64.223,
        'r_14' => 66.666,
        'r_15' => 66.889,
        'r_16' => 66.889,
        'r_17' => 68.000),
    array('name' => 'Audiología, otoneurología y foniatría',
        'r_13' => 65.333,
        'r_14' => 65.333,
        'r_15' => 67.333,
        'r_16' => 67.333,
        'r_17' => 68.222),
    array('name' => 'Calidad de atención clínica',
        'r_13' => 63.110,
        'r_14' => 64.000,
        'r_15' => 64.444,
        'r_16' => 68.667,
        'r_17' => 65.333),  
    array('name' => 'Cirugía general',
        'r_13' => 70.223,
        'r_14' => 72.666,
        'r_15' => 72.766,
        'r_16' => 73.111,
        'r_17' => 75.111),
    array('name' => 'Epidemiología',
        'r_13' => 60.667,
        'r_14' => 62.666,
        'r_15' => 64.668,
        'r_16' => 64.889,
        'r_17' => 67.111),  
    array('name' => 'Genética médica',
        'r_13' => 67.556,
        'r_14' => 69.333,
        'r_15' => 69.333,
        'r_16' => 68.667,
        'r_17' => 69.556), 
    array('name' => 'Geriatría',
        'r_13' => 68.666,
        'r_14' => 70.445,
        'r_15' => 71.777,
        'r_16' => 69.556,
        'r_17' => 70.444),  
    array('name' => 'Ginecología y obstetricia',
        'r_13' => 67.555,
        'r_14' => 69.112,
        'r_15' => 69.333,
        'r_16' => 68.889,
        'r_17' => 71.556),
    array('name' => 'Imagenología diagnóstica y terapéutica',
        'r_13' => 63.999,
        'r_14' => 65.555,
        'r_15' => 66.001,
        'r_16' => 66.889,
        'r_17' => 68.444), 
    array('name' => 'Medicina de la actividad física y deportiva',
        'r_13' => 68.445,
        'r_14' => 73.111,
        'r_15' => 73.111,
        'r_16' => 73.778,
        'r_17' => 74.889),
    array('name' => 'Medicina de rehabilitación',
        'r_13' => 66.666,
        'r_14' => 68.889,
        'r_15' => 69.334,
        'r_16' => 68.444,
        'r_17' => 68.000),
    array('name' => 'Medicina de urgencias',
        'r_13' => 63.777,
        'r_14' => 65.778,
        'r_15' => 65.778,
        'r_16' => 61.556,
        'r_17' => 64.222),
    array('name' => 'Medicina del trabajo y ambiental',
        'r_13' => 64.223,
        'r_14' => 65.333,
        'r_15' => 68.888,
        'r_16' => 63.333,
        'r_17' => 68.000),
    array('name' => 'Medicina familiar',
        'r_13' => 55.555,
        'r_14' => 56.890,
        'r_15' => 57.111,
        'r_16' => 56.667,
        'r_17' => 55.778),
    array('name' => 'Medicina interna',
        'r_13' => 70.667,
        'r_14' => 72.222,
        'r_15' => 72.000,
        'r_16' => 72.444,
        'r_17' => 74.000),
    array('name' => 'Medicina legal',
        'r_13' => 65.333,
        'r_14' => 64.667,
        'r_15' => 68.444,
        'r_16' => 67.778,
        'r_17' => 68.222),
    array('name' => 'Medicina nuclear e imagenología molecular',
        'r_13' => 65.778,
        'r_14' => 66.444,
        'r_15' => 68.222,
        'r_16' => 68.667,
        'r_17' => 68.000),
    array('name' => 'Medicina preventiva',
        'r_13' => 54.667,
        'r_14' => 55.999,
        'r_15' => 57.778,
        'r_16' => 67.333,
        'r_17' => 67.333),
    array('name' => 'Neumología',
        'r_13' => 69.555,
        'r_14' => 71.999,
        'r_15' => 71.334,
        'r_16' => 68.444,
        'r_17' => 73.556),
    array('name' => 'Oftalmología',
        'r_13' => 72.223,
        'r_14' => 74.222,
        'r_15' => 75.333,
        'r_16' => 75.111,
        'r_17' => 76.444),
    array('name' => 'Otorrinolaringología y cirugía de cabeza y cuello',
        'r_13' => 72.445,
        'r_14' => 74.000,
        'r_15' => 73.778,
        'r_16' => 74.889,
        'r_17' => 76.667),
    array('name' => 'Patología clínica',
        'r_13' => 62.445,
        'r_14' => 63.333,
        'r_15' => 66.222,
        'r_16' => 60.000,
        'r_17' => 67.556),
    array('name' => 'Pediatría',
        'r_13' => 66.666,
        'r_14' => 68.445,
        'r_15' => 68.667,
        'r_16' => 69.111,
        'r_17' => 71.111),
    array('name' => 'Psiquiatría',
        'r_13' => 67.112,
        'r_14' => 68.667,
        'r_15' => 69.333,
        'r_16' => 68.444,
        'r_17' => 70.444),
    array('name' => 'Radio oncología',
        'r_13' => 66.000,
        'r_14' => 67.556,
        'r_15' => 68.888,
        'r_16' => 62.000,
        'r_17' => 72.000),
    array('name' => 'Traumatología y ortopedia',
        'r_13' => 68.666,
        'r_14' => 70.223,
        'r_15' => 70.890,
        'r_16' => 70.444,
        'r_17' => 71.111),
        
    );


if(isset($_POST['submit'])){
    $especialidad = $_POST['especialidades'];  // Storing Selected Value In Variable
    $puntaje = $_POST['number_input'];
  	switch($especialidad){
        case "Anatomía Patológica":
      	    get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Anestesiología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Audiología, otoneurología y foniatría":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Calidad de la atención clínica":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Cirugía general":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Epidemiología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Genética médica":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Imagenología diagnóstica y terapéutica":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina de la actividad física y deportiva":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina de rehabilitación":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina de urgencias":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina del trabajo y ambiental":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina familiar":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina interna":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina legal":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina nuclear e imagenología molecular":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina preventiva":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Neumología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Oftalmología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Otorrinolaringología y cirugía de cabeza y cuello":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Patología clínica":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Pediatría":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Psiquiatría":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Radio oncología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Traumatología y ortopedia":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
    
        }
    } 

function get_results_years($esp,$pun,$results){
    $index = array_search($esp, array_column($results, 'name'));
  	echo '<div class="container" style="background-color:rgba(255,255,255,0.85);">';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-xl-4 col-12" style="text-align: center;">';
    echo 'Especialidad: '.$esp;
  	echo '</div>';
  	echo '<div class col-1></div>';
  	echo '<div class="col-xl-4 col-12" style="text-align: center;">';
    echo 'Puntaje obtenido: '.$pun;
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-xl-6 col-12" style="text-align: center;">';
    echo '<ul class="list-group list-group-flush" style="background-color: transparent; margin-left:-1em;">';
  
    if($pun >= $results[$index]['r_13']){
      	$min = $results[$index]['r_13'];
      	echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'Seleccionado ENARM año 2013';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_13'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM año 2013';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_14']){
        $min = $results[$index]['r_14'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'Seleccionado ENARM año 2014';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_14'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM año 2014';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_15']){
        $min = $results[$index]['r_15'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'Seleccionado ENARM año 2015';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_15'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM año 2015';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_16']){
        $min = $results[$index]['r_16'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'Seleccionado ENARM año 2016';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_16'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM año 2016';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_17']){
        $min = $results[$index]['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'Seleccionado ENARM año 2017';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM año 2017';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    echo '</ul>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col" style="height:30px;"></div>';
    echo '</div>';
  	echo '</div>';
  	echo '<div class="row">';
    echo '<div class="col" style="height:30px;"></div>';
    echo '</div>';

    
    //echo $results[$index]['r_13'].'</br>';
    //echo $results[$index]['r_14'].'</br>';
    //echo $results[$index]['r_15'].'</br>';
    //echo $results[$index]['r_16'].'</br>';
    //echo $results[$index]['r_17'].'</br>';
    }

function get_results_2017($pun,$results){
  	echo '<div class="container" style="background-color:rgba(255,255,255,0.85);">';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-6 col-sm-12" style="text-align: center;">';
    echo 'Tu puntaje segun los resultados 2017, pudiste ser seleccionado en: </br>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-6 col-sm-12" style="text-align: center;">';
    echo '<ul class="list-group list-group-flush" style="background-color: transparent; margin-left:-1em;">';
  
  foreach($results as $res){
    if($pun >= $res['r_17']){
        $min = $res['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo '<span style="float:left;">'.$res['name'].'</br></span>';
        echo '<span class="badge badge-primary badge-pill" style="float:right;">Min '.$min.'</span>';
        echo '</li>';
    }
  	
      
  }
    echo '</ul>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
  	echo '</div>';
  	echo '<div class="row">';
    echo '<div class="col" style="height:100px;"></div>';
    echo '</div>';
    
}


?>