<?php
$results = array(
	array('name' => 'Anatomía Patológica',
        'r_13' => 69.778,
        'r_14' => 67.778,
        'r_15' => 71.556,
        'r_16' => 72.667,
        'r_17' => 72.667),
	array('name' => 'Anestesiología',
        'r_13' => 68.000,
        'r_14' => 70.666,
        'r_15' => 69.333,
        'r_16' => 67.556,
        'r_17' => 69.556),  
    array('name' => 'Cirugía general',
        'r_13' => 70.444,
        'r_14' => 72.667,
        'r_15' => 73.110,
        'r_16' => 73.111,
        'r_17' => 75.111), 
    array('name' => 'Genética médica',
        'r_13' => 68.221,
        'r_14' => 70.666,
        'r_15' => 69.333,
        'r_16' => 68.667,
        'r_17' => 72.889),  
    array('name' => 'Ginecología y obstetricia',
        'r_13' => 67.556,
        'r_14' => 70.000,
        'r_15' => 69.334,
        'r_16' => 69.333,
        'r_17' => 71.556),
    array('name' => 'Imagenología diagnóstica y terapéutica',
        'r_13' => 65.334,
        'r_14' => 70.889,
        'r_15' => 67.778,
        'r_16' => 68.222,
        'r_17' => 70.000),
    array('name' => 'Medicina de rehabilitación',
        'r_13' => 70.445,
        'r_14' => 69.779,
        'r_15' => 00.000,
        'r_16' => 71.556,
        'r_17' => 69.333),
    array('name' => 'Medicina de urgencias',
        'r_13' => 64.667,
        'r_14' => 68.000,
        'r_15' => 67.556,
        'r_16' => 63.778,
        'r_17' => 64.444),
    array('name' => 'Medicina familiar',
        'r_13' => 00.000,
        'r_14' => 00.000,
        'r_15' => 62.222,
        'r_16' => 00.000,
        'r_17' => 58.222),
    array('name' => 'Medicina interna',
        'r_13' => 71.778,
        'r_14' => 72.222,
        'r_15' => 72.000,
        'r_16' => 72.444,
        'r_17' => 74.444),
    array('name' => 'Medicina nuclear e imagenología molecular',
        'r_13' => 66.888,
        'r_14' => 69.333,
        'r_15' => 00.000,
        'r_16' => 00.000,
        'r_17' => 71.333),
    array('name' => 'Oftalmología',
        'r_13' => 72.888,
        'r_14' => 76.667,
        'r_15' => 77.778,
        'r_16' => 75.333,
        'r_17' => 76.444),
    array('name' => 'Otorrinolaringología y cirugía de cabeza y cuello',
        'r_13' => 72.667,
        'r_14' => 74.889,
        'r_15' => 74.444,
        'r_16' => 76.889,
        'r_17' => 76.667),
    array('name' => 'Pediatría',
        'r_13' => 66.667,
        'r_14' => 69.999,
        'r_15' => 68.889,
        'r_16' => 69.111,
        'r_17' => 71.333),
    array('name' => 'Psiquiatría',
        'r_13' => 67.778,
        'r_14' => 69.333,
        'r_15' => 69.555,
        'r_16' => 00.000,
        'r_17' => 72.222),
    array('name' => 'Traumatología y ortopedia',
        'r_13' => 68.666,
        'r_14' => 70.444,
        'r_15' => 71.111,
        'r_16' => 70.444,
        'r_17' => 71.333),
        
    );


if(isset($_POST['submit'])){
    $especialidad = $_POST['especialidades'];  // Storing Selected Value In Variable
    $puntaje = $_POST['number_input'];
  	switch($especialidad){
        case "Anatomía Patológica":
      	    get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Anestesiología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Cirugía general":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Genética médica":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Ginecología y obstetricia":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Imagenología diagnóstica y terapéutica":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina de rehabilitación":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina de urgencias":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina familiar":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina interna":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Medicina nuclear e imagenología molecular":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Oftalmología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Otorrinolaringología y cirugía de cabeza y cuello":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Pediatría":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Psiquiatría":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Traumatología y ortopedia":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
    
        }
    } 

function get_results_years($esp,$pun,$results){
    $index = array_search($esp, array_column($results, 'name'));
  	echo '<div class="container" style="background-color:rgba(255,255,255,0.85);">';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-xl-4 col-12" style="text-align: center;">';
    echo 'Especialidad: '.$esp;
  	echo '</div>';
  	echo '<div class col-1></div>';
  	echo '<div class="col-xl-4 col-12" style="text-align: center;">';
    echo 'Puntaje obtenido: '.$pun;
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-xl-6 col-12" style="text-align: center;">';
    echo '<ul class="list-group list-group-flush" style="background-color: transparent; margin-left:-1em;">';
  
     if($pun >= $results[$index]['r_17']){
        if($results[$index]['r_17'] == 0){
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'No disponible ENARM 2017';
            echo '<span class="badge badge-primary badge-pill">No disponible</span>';
            echo '</li>';
        } else {
            $min = $results[$index]['r_17'];
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'Seleccionado ENARM 2017';
            echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
            echo '</li>';
        }
    } else {
        $min = $results[$index]['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM 2017';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
  
    if($pun >= $results[$index]['r_16']){
        if($results[$index]['r_16'] == 0){
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'No disponible ENARM 2016';
            echo '<span class="badge badge-primary badge-pill">No disponible</span>';
            echo '</li>';
        } else {
            $min = $results[$index]['r_16'];
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'Seleccionado ENARM 2016';
            echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
            echo '</li>';
        }
    } else {
        $min = $results[$index]['r_16'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM 2016';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
  
    if($pun >= $results[$index]['r_15']){
        if($results[$index]['r_15'] == 0){
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'No disponible ENARM 2015';
            echo '<span class="badge badge-primary badge-pill">No disponible</span>';
            echo '</li>';
        } else {
            $min = $results[$index]['r_15'];
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'Seleccionado ENARM 2015';
            echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
            echo '</li>';
        }
    } else {
        $min = $results[$index]['r_15'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM 2015';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
  
    if($pun >= $results[$index]['r_14']){
        if($results[$index]['r_14'] == 0){
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'No disponible ENARM 2014';
            echo '<span class="badge badge-primary badge-pill">No disponible</span>';
            echo '</li>';
        } else {
            $min = $results[$index]['r_14'];
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'Seleccionado ENARM 2014';
            echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
            echo '</li>';
        }
    } else {
        $min = $results[$index]['r_14'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM 2014';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
  
    if($pun >= $results[$index]['r_13']){
        if($results[$index]['r_13'] == 0){
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'No disponible ENARM 2013';
            echo '<span class="badge badge-primary badge-pill">No disponible</span>';
            echo '</li>';
        } else {
            $min = $results[$index]['r_13'];
            echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
            echo 'Seleccionado ENARM 2013';
            echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
            echo '</li>';
        }
    } else {
        $min = $results[$index]['r_13'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo 'No seleccionado ENARM 2013';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }




    echo '</ul>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col" style="height:30px;"></div>';
    echo '</div>';
  	echo '</div>';
  	echo '<div class="row">';
    echo '<div class="col" style="height:30px;"></div>';
    echo '</div>';

    
    //echo $results[$index]['r_13'].'</br>';
    //echo $results[$index]['r_14'].'</br>';
    //echo $results[$index]['r_15'].'</br>';
    //echo $results[$index]['r_16'].'</br>';
    //echo $results[$index]['r_17'].'</br>';
    }

function get_results_2017($pun,$results){
  	echo '<div class="container" style="background-color:rgba(255,255,255,0.85);">';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-6 col-sm-12" style="text-align: center;">';
    echo 'Tu puntaje segun los resultados 2017, pudiste ser seleccionado en: </br>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-6 col-sm-12" style="text-align: center;">';
    echo '<ul class="list-group list-group-flush" style="background-color: transparent; margin-left:-1em;">';
  
  foreach($results as $res){
    if($pun >= $res['r_17']){
        $min = $res['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center" style="background-color: transparent;">';
        echo $res['name'].'</br>';
        echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
        echo '</li>';
    }
  	
      
  }
    echo '</ul>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
  	echo '</div>';
  	echo '<div class="row">';
    echo '<div class="col" style="height:100px;"></div>';
    echo '</div>';
    
}

?>