# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import simplejson as json
import PyPDF2
from PyPDF2 import PdfFileWriter
import pdfkit 
from fpdf import FPDF 
print u"åäö"
def load_strings():
    pass



contract_string = [[u'','ln'],
				[u'CONTRATO MERCANTIL DE RENTA DE SERVICIOS DE PLATAFORMA DIGITAL','tcsn'],
				[u'','ln'],
                [u'QUE CELEBRAN POR UNA PARTE LA MORAL SERVICIOS MEDIK AWAY S.A.P.I de C.V QUIEN EN LO SUCESIVO PARA LOS EFECTOS DEL PRESENTE CONTRATO SE LE DENOMINARA "LA EMPRESA" REPRESENTADA EN  EL PRESENTE CONTRATO POR SU APODERADO Y REPRESENTANTE LEGAL EL C. JOSÉ DAVID PIÑA WONG Y POR LA OTRA PARTE  EL C. <name>, QUIEN EN PARA LOS EFECTOS DEL PRESENTE CONTRATO SE LE DENOMINARA "EL MEDICO", ASI MISMO PARA AL PRESENTE DOCUMENTO SE LE DENOMINARA "CONTRATO".  ','nor'],
				[u'Ambas partes manifiestan su conformidad en sujetarse al tenor de las siguientes declaraciones y cláusulas.','nor'],
				[u'','ln'],
				[u'DECLARACIONES','tcn'],
				[u'','ln'],
				[u'I.- Declara LA EMPRESA ','n'],
				[u'A) Ser  una empresa legalmente constituida conforme a las leyes mexicanas, ser una empresa dedicada a todas y cada una de las actividades que establecen en el objeto social de la moral del acta constitutiva cuya razón social es "SERVICIOS MEDIK AWAY S.A.P.I de C.V",  siendo estos desarrollo de sistemas software de comunicación y la comercialización de los mismos, prestación de servicios de comunicación realización de transacciones de monetarias celebradas por terceros por los servicios prestados a través de plataformas digitales orientadas a la industria  medica, comercialización de espacios publicitarios e dichas plataformas, prestación de servicios y promociones medicas y de salud a terceros a través de las plataformas orientadas a la industria medica, tratamiento de cualquier tipo de información incluyendo datos personales con el fin de emplear  dicha información en las actividades propias de su objeto social o para comercialización en los términos permitidos por la ley.','nor'],
				[u'Representada en este contrato por el C. JOSÉ DAVID PIÑA WONG, el cual tiene plenas facultades otorgadas para poder comprometer en derecho y obligaciones de su poderdante  en el poder notarial mediante el instrumento público número 77,593 de fecha 05/04/2018 pasado ante la fe del notario público  número 109 lic.Luis De Angoitia Becerra.','nor'],
				[u'B) Señala como domicilio la moral SERVICIOS MEDIK AWAY S.A.P.I de C.V,  para los efectos de este contrato el ubicado en: Insurgentes Sur 214, Roma Norte, 06700 Ciudad de México, CDMX. Registro federal de contribuyentes SMA170104I32, correo electrónico contacto@medikaway.com, teléfono (55) 57 05 32 00.','nor'],
				[u'C) Declara la moral SERVICIOS MEDIK AWAY S.A.P.I de C.V, que es el titular del desarrollo del SOFTWARE, denominado PLATAFORMA DE COMUNICACIÓN PARA LA ATENCION MEDICA A DISTANCIA  y del cual tiene todos los derechos de propiedad intelectual para el  uso y explotación comercial de la plataforma digital, de  los cuales están plenamente registrados  ante el INSTITUTO NACIONAL DE DERECHOS DE AUTOR así como INSTITUTO MEXICANO  DE PROPIEDAD  INTELECTUAL, por lo cual  goza de todos los derechos morales y patrimoniales que la Ley Federal del Derecho de Autor le otorga, si ninguna restricción a la moral SERVICIOS MEDIKAWAY S.A.P,I de C.V no teniendo impedimento legal  alguno para ofertar el uso  y explotación sobre los  contratar sobre el particular del SOFTWARE, la cual ofrece  a la comunidad médica  en general a los profesionistas médicos de cualquier área o especialidad de la medicina dentro del territorio nacional de la República Mexicana,  así como cualquier parte del mundo.','nor'],
				[u'Por medio de la cual LA  EMPRESA , otorga en arrendamiento el uso y explotación exclusiva a los MÉDICOS , para que por medio de sus sistemas desarrollados en el SOFTWARE, los MÉDICOS puedan brindar a los pacientes que requieran sus servicios por medio de la plataforma digital de comunicación atención médica  a distancia, así como el almacenamiento en sus base de datos de los pacientes en los expedientes clínicos desarrollados, así como el resguardo de  datos tanto personales de los pacientes como de las notas clínicas y expedientes y cualquier información relacionada con la atención médica  general o confidencial que se descargue dentro de la plataforma, la cual estará debidamente almacena en los servidores que tiene contratados LA EMPRESA, con empresas de reconocimiento mundial con los más altos estándares de seguridad para su confidencialidad  del almacenamiento de datos, así mismo dentro de los servicios del SOFTWARE,  se tiene desarrollado formato de receta electrónica dentro de la plataforma.   ','nor'],
				[u'D.- Declara LA EMPRESA  que el objeto y finalidad del presente contrato  es regular el arrendamiento de programas informáticos a cambio de una contraprestación. El arrendador se dedica a realizar programas informáticos que, posteriormente y a través de este documento, arrienda a aquellos interesados en los mismos, incluyendo el mantenimiento mediante un equipo de profesionales.','nor'],
				[u'','ln'],
				[u'II.- Declara  EL MEDICO ,  ','n'],
				[u'A.-Que es una persona física  con la profesión de médico general el cual está debidamente titulado y cuenta con cédula profesional para el debido ejercicio de la profesión de médico, el cual ofrece sus servicios al público en general  en su domicilio legal, ','nor'],
				[u'B) EL MEDICO , manifiesta que tiene por nombre <name>  el cual se identifica por medio de su credencial para votar expedida por Instituto Nacional Electoral  con número <id_number>, acredita su profesión con la cédula profesional número <p_license> expedida por Secretaría de Educación Pública que tiene su domicilio en las calles de <address>, registro federal de causante  <rfc>, nacionalidad  mexicana, teléfono <phone>, correo electrónico <email>, número de identificación Medikaway "MDK-M-__", número de la cuenta bancaria_______________________ clave de interbancaria ________________________________ nombre de institución bancaria ______________________________________________ .','n'],
				[u'C)  EL MEDICO ,  declara que acepta el presente contrato de  arrendamiento  de SOFTWARE,  para brindar a los pacientes que requieran sus servicios por medio de la plataforma digital de comunicación atención médica  a distancia  de la cual tiene los derechos de uso explotación la moral SERVICIOS MEDIKAWAY S.A.P.I. de C.V','nor'],
				[u'Hechas las declaraciones anteriores, ambas partes convienen en otorgar las siguientes: ','nor'],
				[u'','ln'],
				[u'III.- AMBAS PARTES.- ','n'],
				[u'Manifiestan que entre ambos  en el presente contrato no existe ninguna relación de índole laboral  que los una, únicamente relación jurídica de carácter  civil, mercantil de arrendamiento de SOFTWARE, por medio de la cual  EL MEDICO  brindara servicios de atención médica  a distancia  a los pacientes que requieran sus servicios  por medio de la plataforma de comunicación de la EMPRESA, teniendo plena capacidad jurídica ambas parte  para contratar y obligarse conforme a ley  y los servicios profesionales.','nor'],
				[u'','ln'],
				[u'C L A U S U L A S','tnc'],
				[u'','ln'],
				[u'PRIMERA.- DESCRIPCION DEL SERVICIO. ','n'],
				[u'"LA EMPRESA", se compromete  a brindar  mediante el pago de la retribución pactada en el presente contrato, el uso y explotación  de sus servicios y desarrollos de la plataforma de comunicación digital móvil en tiempo real para que brinde EL  MEDICO, atención médica a distancia  a los  usuarios o pacientes que soliciten sus servicios por  medio de la plataforma digital a través  de la  aplicación móvil que descarguen los usuarios o pacientes.','nor'],
				[u'El servicio de la plataforma es  ilimitado durante los 365 días del año durante el horario  de las 24 horas del día.','nor'],
				[u'En el  desarrollo de la plataforma digital de LA EMPRESA se tiene diseñado un archivo que contiene el expediente clínico el cual  EL MEDICO, podrá utilizar para llevar los registros de todos los pacientes que este atienda por medio de la plataforma digital, el cual podrá tener acceso en todo tiempo, así como el diseño predefinido de las recetas para que se facilite su registro al igual tendrá acceso en todo momento que lo considere. ','nor'],
				[u'Toda la información  tanto confidencial  de sus pacientes y de los registros que  EL  MEDICO  haga de las atenciones médicas a distancia quedara resguardadas en los servidores de la EMPRESA, por el termino de 3 años con lo cual se brinda una garantía al MEDICO, por cualquier contratiempo que pudiera surgir.','nor'],
				[u'LA EMPRESA, tiene establecido un sistema de administración de pagos por  medio de una pasarela de pagos con la empresa GRUPO CONEKTAME S.A de .C.V, por la cual se garantiza el pago de  los honorarios médicos por el servicio brindado por medio de la plataforma digital, mismos pagos que serán realizados por los pacientes por medio de tarjeta bancaria de débito o crédito en forma directa en la plataforma.','nor'],
				[u'EL MEDICO, que contrate los servicios digitales de comunicación de LA EMPRESA, será registrado con todos sus datos de identificación personal nombre, fotografía, cédula profesional característica de área médica o cualquier otro dato que EL MEDICO determine   dentro de la aplicación móvil  la cual se publicaran  sus servicios como médico para ofrecer sus servicios médicos al público usuarios de la aplicación  móvil de dentro del desarrollo de la plataforma de comunicación. ','nor'],
				[u'','ln'],
				[u'SEGUNDA.-UBICACION DE ACTIVIDADES. ','n'],
				[u'Las actividades descritas se desarrollaran en las instalaciones de "LA EMPRESA", mediante la utilización de la  infraestructura y sistemas informáticos del mismo con  personal altamente calificado el cual depende de  "LA EMPRESA", así como con recurso propios de este,  sin tener ninguna relación de subordinación o dependencia económica con el MEDICO. ','nor'],
				[u'','ln'],
				[u'TERCERA.- RELACION. ','n'],
				[u'La relación entre las partes tiene exclusivamente carácter mercantil, no existiendo vínculo laboral o de subordinación  alguno entre "EL MEDICO",  LA EMPRESA o el personal de esta   que eventualmente este prestando sus servicios en el domicilio social de aquel.','nor'],
				[u'','ln'],
				[u'CUARTA.- OBLIGACIONES.','n'],
				[u'"LA EMPRESA" se obliga a desarrollar las actividades mencionadas en la cláusula primera con toda diligencia a efecto de  cumplir plenamente con "EL  MEDICO" estableciéndose como parámetros de cumplimiento y pericia los que regularmente se manejan en el mercado, obligándose a aportar toda su experiencia y capacidad, dedicando todo el tiempo que sea necesario para dar cumplimiento al presente contrato.','nor'],
				[u' "LA EMPRESA", se obliga a informar a "EL MEDICO" del estado que guarde su labor, cuantas veces sea requerido para ello,  así como de informar mensualmente el número de consultas brindadas por este por medio de la plataforma digital.   ','nor'],
				[u'','ln'],
				[u'QUINTA.- VIGENCIA ','n'],
				[u'El presente contrato estará vigente a partir del día <day> mes <month> año <year> y será por tiempo indefinido, el cual podrá darse por terminado en cualquier momento por cualquiera de las partes sin ninguna responsabilidad para ambas, con la única condición que lo haga sabe con una anticipación de 30 días naturales y que no se adeuden cantidad alguna  a LA EMPRESA , por el servicio brindado o que se adeude cantidad alguna por parte de LA EMPRESA , al MEDICO por motivo de pago de servicios médicos por medio de la plataforma, en cuyo caso de tener algún adeudo por alguna de las partes se tendrá que liquidar previamente a la terminación del contrato. De no ser  saldado el adeudo la parte acreedora del adeudo ejercitará acción legal para exigir el pago, así como el pago de los daños y perjuicios que se originen por motivo del incumplimiento.  ','nor'],
				[u'','ln'],
				[u'SEXTA.- PRECIO Y FORMA DE PAGO.','n'],
				[u'Ambas partes aceptan que el pago por los servicios que  LA EMPRESA  otorgara al MEDICO, por los servicios de la plataforma de comunicación por cada cobro de consulta que el medico otorgue por medio de la plataforma, LA EMPRESA, cobrara un 30% sobre el costo de consulta por dicho servicio.','nor'],
				[u'El cobro directo de la consulta lo realizará  LA EMPRESA, por medio de la plataforma de comunicación y que realice el paciente por medio de tarjeta bancaria de crédito o débito mismo pago que se realizará por medio de una pasarela de pagos de la empresa que tiene contratado LA EMPRESA, con la empresa GRUPO CONEKTAME S.A  de .C.V.','nor'],
				[u' LA EMPRESA , actuará en el presente contrato  únicamente como retenedor del pago de las consultas médicas  y cada día último  de  mes calendario, realizará el corte contable del MEDICO, y realizará la transferencias bancaria a la cuenta bancaria que el médico registre  del pago correspondiente de las consultas brindadas por la plataforma digital, descontando por concepto de pago de servicios de dicha transferencia el porcentaje de la comisión que se pacta por cada consulta del 30% del costo total de la consulta, entregando LA EMPRESA  al MEDICO la factura correspondiente por el cobro de dichos servicios mensualmente. ','nor'],
				[u'El precio que se fija para brindar la consulta médica por medio de la plataforma digital y que las partes están de acuerdo en aceptar de $ 250.00 por consulta, en caso de que se pacte una cantidad mayor a dicho precio por el MEDICO, se agregara en un anexo al presente contrato con dicha modificación. ','nor'],
				[u'','ln'],
				[u'SEPTIMA.- PAGO DE IMPUESTOS.','n'],
				[u'La EMPRESA durante los primeros 5 días de cada mes calendario realizara la transferencia del  pago de los honorarios de los MEDICOS, que obtuvieron en el mes calendario inmediato anterior, por las atenciones médicas que realizo este por medio de la plataforma de comunicación,  mismo pago que realizara la EMPRESA mediante trasferencia bancaria a la cuenta  bancaria que el  MEDICO,  haya registrado ante la EMPRESA, estableciendo que la empresa en el presente contrato actúa únicamente  como retenedor temporal de los ingresos del MEDICO, en el mes calendario motivo por el cual no será considerado como ingreso para la EMPRESA, lo único  que es considerado como ingreso para esta es el porcentaje pactado como pago por el uso de la plataforma siendo este el 30% más IVA     de cada consulta que el  MEDICO brinde por medio de esta  de lo cual será facturado al médico mensualmente para los efecto fiscales correspondientes.','nor'],
				[u'EL MEDICO manifiesta que con motivo de los servicios profesionales  que este brinde por medio de la plataforma digital de la EMPRESA, y de los cuales acepta que la EMPRESA actué en el presente contrato como intermediario y retenedor  para efectos del  pago de los honorarios que hagan los pacientes por medio de la pasarela de pago de la empresa GRUPO CONEKTA S.A de C.V, motivo por el cual EL MEDICO, acepta que este es el único responsable del pago de los impuesto correspondientes por los ingresos que obtenga por sus servicios prestado  a través de la plataforma de comunicación de la EMPRESA. ','nor'],
				[u'Liberando de cualquier responsabilidad legal o pago de algún impuesto a la empresa toda vez que esta actúa únicamente en el presente contrato como intermediario y retenedor provisional del pago de los ingresos que obtenga el MEDICO en el presente contrato. ','nor'],
				[u'Cuando el PACIENTE requiera factura por los honorarios médicos este lo requerirá al momento que solicite el servicio por medio de la plataforma digital, para que proporcione sus datos fiscales para la emisión de la factura por honorarios por los servicios otorgados por el MEDICO, la factura se emitirá dentro de los 5 días hábiles posteriores por el MEDICO, el cual la canalizara por medio de la EMPRESA, para que se le haga llegar al PACIENTE, al correo electrónico que haya registrado este con todos los requisitos fiscales que las leyes hacendarias establecen. ','nor'],
				[u'','ln'],
				[u'OCTAVA.- RESPONSABILIDADES DE PARTES.','n'],
				[u'-EL MEDICO, manifiesta que será el único responsable de cualquier negligencia médica que se llegara a generar por  la atención médica a distancia que otorgue por medio del plataforma de comunicación de LA EMPRESA, toda vez que la plataforma digital de comunicación es únicamente un medio de comunicación y con ello LA EMPRESA no tendrá ninguna responsabilidad civil o penal como consecuencia de alguna negligencia médica o violación al reglamento de funcionamiento , en cuyo caso EL MEDICO deslinda de cualquier responsabilidad a LA EMPRESA.','nor'],
				[u'-LA EMPRESA, manifiesta que si por alguna circunstancias imputable a ella por sus sistemas  informáticos se llegare a generar algún daño en su patrimonio a EL MEDICO, esta se obliga a resarcir el costo del daño, siempre y cuando se acredite por EL MEDICO, el daño, si por alguna circunstancia de que se llegare a suspender el servicio de comunicación por causa  ajena a  la EMPRESA por causa mayor ajena a la colectada de LA EMPRESA, no será responsable de ningún pago o penalidad.  ','nor'],
				[u'-LA EMPRESA, manifiesta que si por alguna negligencia justificada  de alguno de sus empleados se incumpliera la cláusula de confidencialidad del presente contrato será responsable del pago del daño y perjuicios que se llegaran a causar al MEDICO, independientemente de las sanciones civiles o penales que incurra el personal de la EMPRESA.   ','nor'],
				[u'','ln'],
				[u'NOVENA .- DE CONFIDENCIALIDAD','n'],
				[u'LA EMPRESA.- se obliga a resguardar en sus sistemas informáticos toda aquella información personal general y sensible de EL MEDICO y del  PACIENTES que les brinde la atención medica distancia por medio de la plataforma de comunicación ello de acuerdo a lo establecido en la LEY DE PROTECCION DE DATOS PERSONALES EN POSESIÓN DE PARTICULARES, los cuales no podrán ser transferidos, divulgados, donados o vendidos, a terceros sin la autorización del titular, así mismo no divulgara la información de las notas y recetas que el medico registre dentro de la plataforma de comunicación sin el consentimiento de EL MEDICO, en caso de que alguna autoridad competente requiera alguna información confidencial se obliga hacer del conocimiento del titular dicho requerimiento para los efectos legales correspondientes. ','nor'],
				[u' AMBAS PARTES.- aceptan que los datos personales y médicos de los pacientes NO serán divulgados por ninguna de las partes bajo ninguna circunstancia respetando el derecho de confidencialidad que la ley establece ; cada una de las partes se obligan a no divulgar a terceros ninguna información concerniente a sus negocios, clientes, secretos industriales y comerciales, métodos, procesos, procedimientos o cualquier otra información confidencial, a excepción de aquella información que sea de dominio público o mediante previo consentimiento por escrito de la otra parte.','nor'],
				[u'"LA EMPRESA" y "EL MEDICO", aceptan y se obligan expresamente a que si llegaran a faltar a su obligación de confidencialidad de manera directa o indirecta por cualquier medio ya sea oral, escrito o electrónico, tendrán que pagar por todos los daños  y perjuicios que le pudieran causar a la otra parte.','nor'],
				[u'','ln'],
				[u'DECIMA .- LICENCIA Y CONDICIONES DE USO.  ','n'],
				[u'El servicio otorgado a través de la plataforma digital es propiedad de "LA EMPRESA", y se otorga su licencia con carácter personal al MEDICO con el cual se firme el presente contrato exclusivo y no sub-licenciable de acuerdo con los términos y las condiciones que se incluyen en este CONTRATO.','nor'],
				[u'Estas condiciones definen los servicios y usos legales de LA EMPRESA, todas las actualizaciones, revisiones, sustituciones y cualquier copia del servicio de la plataforma de comunicación  que se hagan para los usuarios.','nor'],
				[u' La información cargada por los usuarios de la plataforma de comunicación  es propiedad de ellos mismos y en ningún momento será propiedad de LA EMPRESA.','nor'],
				[u' "LA EMPRESA" se reserva todos los derechos que no se haya otorgado a EL MEDICO de manera explícita en el presente documento.','nor'],
				[u'','ln'],
				[u'DECIMA PRIMERA.- CANCELACION Y SUSPENSIONES.','n'],
				[u'La licencia  otorgada para el uso de la plataforma digital de comunicación a EL MEDICO se podrá dar por cancelada o suspendida por LA EMPRESA, por violar el reglamento de funcionamiento anexo a este contrato. ','nor'],
				[u'','ln'],
				[u'DECIMA SEGUNDA  - APLICACIÓN LEGAL ','n'],
				[u'Ambas partes convienen desde ahora que en caso de conflicto derivado de la aplicación o interpretación de este contrato, se sujetarán al procedimiento de, se someten expresamente a la jurisdicción de los Tribunales competentes estatales en la Ciudad de México, Distrito Federal, renunciando en forma expresa a cualquier fuero que por razón de domicilio, vecindad o nacionalidad pudiera corresponderles.','nor'],
				[u'Leído que fue por ambas partes el presente contrato y enteradas de su contenido significado y alcance legal, no habiendo vicio alguno que impida su celebración y debido cumplimiento, lo firman al margen  por duplicado las cuatro fojas que integran el presente contrato  en la Ciudad de México, a los <day> días del mes de <month> del año <year>, quedando un ejemplar original en poder de cada una de las partes.  y los testigos de constancia del presente contrato los C. ERICK DANIEL  MIRANDA VARAS  y el  C. VICTOR MANUEL MIRANDA MONTES.','nor'],
				[u' ','ln'],
				[u' ','ln'],
				[u' ','ln'],
				[u'LA EMPRESA','c'],
				[u'SERVICIOS MEDIKAWAY S.A.P.I. de C.V.','c'],
				[u'REPRESENTANTE','c'],
				[u'José David Piña Wong','c'],
				[u'','ln'],
				[u'','ln'],
				[u'','ln'],
				[u'','ln'],
				[u'','ln'],
				[u'EL MEDICO','c'],
				[u'El C. <name>','c'],
				[u'','ln'],
				[u'','ln'],
				[u'','ln'],
				[u'                    TESTIGO.                                                                        TESTIGO.                             ','f2'],
				[u'EL C. ERICK DANIEL MIRANDA VARAS      EL C. VICTOR MANUEL MIRANDA MONTES','f2']
				]



string2 = u'ñáéíóú ÁÉÍÓÚÑ'
writer=PdfFileWriter()
pdf = FPDF()
pdf.add_page()
pdf.set_right_margin(25.4)
pdf.set_left_margin(25.4)
pdf.set_top_margin(25.4)
pdf.set_auto_page_break(1,25.4)
pdf.set_font('Arial','',11)

def string_style(string):
	if string[1] == 'n':
		pdf.set_font('Arial','B',11)
		pdf.multi_cell(0,5,string[0])
	elif string[1] == 'nor':
		pdf.set_font('Arial','',11)
		pdf.multi_cell(0,5,string[0])
	elif string[1] == 'tcsn':
		pdf.set_font('Arial','BU',11)
		pdf.multi_cell(0,5,string[0],0,'C')
	elif string[1] == 'tcn':
		pdf.set_font('Arial','B',11)
		pdf.multi_cell(0,5,string[0],0,'C')
	elif string[1] == 'c':
		pdf.set_font('Arial','',11)
		pdf.multi_cell(0,4,string[0],0,'C')
	elif string[1] == 'f2':
		pdf.set_font('Arial','',11)
		pdf.multi_cell(0,4,string[0])
	elif string[1] == 'ln':
		pdf.ln()


	

for string in contract_string:
	pdf.set_font('Arial','',11)
	string_style(string)
	pdf.ln()



pdf.output('tuto1.pdf','F')
writer.addBlankPage(220/0.352778,280/0.352778)

outfp=open("file.pdf",'wb')
writer.write(outfp)


#Debo crear una función que filtre si la madre que le envío es un párrafo, un título o algo especial
#Debo hacer una segunda función que reemplace con los datos necesarios