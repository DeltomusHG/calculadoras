# -*- coding: UTF-8 -*-
import hashlib


def get_hash(password):
    return hashlib.sha224('{}'.format(password)).hexdigest()