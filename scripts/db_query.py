# -*- coding: UTF-8 -*-
import psycopg2, psycopg2.extras, xlwt, xlrd, json, hash_generator
from collections import OrderedDict
from datetime import datetime
import simplejson as json

# def login(user_email,user_password):
#     user_hash = hash_generator.get_hash(user_password)
#     db_response = OrderedDict()
#     try:
#         cur.execute("""
#         SELECT id,name,surname 
#         FROM users
#         WHERE  email = '{}' and hash = '{}'
#         """.format(user_email,user_hash))
#     except:
#         db_response['error'] = 'Error: No se pudo conectar con la base de datos.'
    
#     if cur.rowcount != 0:
#         for row in cur:
#             db_response['id'] = row[0]
#             db_response['name'] = row[1]
#             db_response['surname'] = row[2]
#     else:
#         db_response['id'] = 0
#     return db_response

    
#Creating db connection and cursor
# try:
#     conn = psycopg2.connect(database = 'support_system', user = 'master_backoffice', password = 'Pa55word', host = 'medikaway-backoffice.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
#     cur = conn.cursor()
#     #print login('leonardo.hernandez@medikaway.com','DeltA.FrosT.05')
# except:
#     print 'Error: No ha sido posible conectar con la base de datos.'
def cec(data):
    system_log = open('system_log.log','a+')
    system_log.write('data: {}\r\n'.format(data)) 
    info = json.loads(data)
    system_log.write('info: {}\r\n'.format(info)) 
    # print info['cartilla']
    try:
        conn = psycopg2.connect(database = 'mawmedcards', user = 'mawdelta', password = 'F_t5EErS25L=zBPV', host = 'mawmedcards.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        return response
    #query = "INSERT INTO test (info) VALUES (0)"
    query = u"""
        INSERT INTO cec(patient_id,peso,imc,cc,ta_sistolica,ta_diastolica,glucemia,hba,rev_pies,colesterol_total,ldl,hdl,trigliceridos,trat_nofarma,trat_farma,pac_controlado,complicaciones,observaciones)
        VALUES     ('{patient_id}','{peso}','{imc}','{cc}','{ta_sistolica}','{ta_diastolica}','{glucemia}','{hba}','{rev_pies}','{colesterol_total}','{ldl}','{hdl}','{trigliceridos}','{trat_nofarma}','{trat_farma}','{pac_controlado}','{complicaciones}','{observaciones}')
    """.format(patient_id = info['id'],peso = info['peso'],imc = info['imc'],cc = info['cc'],
        ta_sistolica = info['sistolica'],ta_diastolica = info['distolica'],glucemia = info['glucemia'],
        hba = info['hba1c'],rev_pies = info['pies'],colesterol_total = info['totalcolesterol'],
        ldl = info['ldl'],hdl = info['hdl'],trigliceridos = info['trigliceridos'],trat_nofarma = info['trat_nofarma'],
        trat_farma = info['trat_farmacologico'],pac_controlado = info['pacientecontrolado'],
        complicaciones = info['complicaciones'],observaciones = info['observaciones'])
    
    try:
        cur.execute(query)
        cur.close()
        conn.close()
        return 'Escritura exitosa'
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        return response

    return 'ok'

def cmeyl(data):
    system_log = open('system_log.log','a+')
    system_log.write('data: {}\r\n'.format(data)) 
    info = json.loads(data)
    system_log.write('info: {}\r\n'.format(info)) 
    try:
        conn = psycopg2.connect(database = 'mawmedcards', user = 'mawdelta', password = 'F_t5EErS25L=zBPV', host = 'mawmedcards.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        return response

    #query = "INSERT INTO test (info) VALUES (1)"
    query = u"""
        INSERT INTO cmeyl(patient_id,gesta,cesareas,abortos,hijos_nacidos_vivos,hijos_nacidos_muertos,tipos_anticonceptivos,tiempo_anticonceptivos,fuma,semanas_gestacion,tiempo_uso,peso,tension_arterial,fondo_uterino,alarma,medicamentos,analisis_clinicos,ego,a_acido_folico,ayuda_alimentaria,observaciones)
        VALUES ('{id}','{gesta}','{cesareas}','{abortos}','{hijos_nacidos_vivos}','{hijos_nacidos_muertos}','{tipos_anticonceptivos}','{tiempo_anticonceptivos}','{fuma}','{semanas_gestacion}','{tiempo_uso}','{peso}','{tension_arterial}','{fondo_uterino}','{alarma}','{medicamentos}','{analisis_clinicos}','{ego}','{a_acido_folico}','{ayuda_alimentaria}','{observaciones}')
    """.format(id = info['id'],gesta = info['gesta'],cesareas = info['cesareas'],abortos = info['abortos'],
    hijos_nacidos_vivos = info['vivos'],hijos_nacidos_muertos = info['muertos'],tipos_anticonceptivos = info['tipoanticonceptivo'],
    tiempo_anticonceptivos = info['tiempodeuso'],fuma = info['habitotabaquico'],semanas_gestacion = info['semanasdegestacion'], 
    tiempo_uso = info['tiempodeuso'],peso = info['pesoembarazada'],tension_arterial = info['tensionarterial'],
    fondo_uterino = info['fondouterino'],alarma = info['signosdealarma'],medicamentos = info['medicamentos'],
    analisis_clinicos = info['analisisclinicos'],ego = info['examenorina'],a_acido_folico = info['acidofolico'],
    ayuda_alimentaria = info['ayudaalimentos'],observaciones = info['observacionesemb'])
    
    try:
        cur.execute(query)
        cur.close()
        conn.close()
        return 'Escritura exitosa'
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        return response

    return 'ok'

def control_infantil(data):
    system_log = open('system_log.log','a+')
    system_log.write('{}\r\n'.format(data)) 
    info = json.loads(data)
    system_log.write('{}\r\n'.format(info)) 
    try:
        conn = psycopg2.connect(database = 'mawmedcards', user = 'mawdelta', password = 'F_t5EErS25L=zBPV', host = 'mawmedcards.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        response = 'Ha habido un error: {} \r\n {} \r\n {}'.format(e,data,info)
        return response

    query = "INSERT INTO test (info) VALUES (2)"
    query = u"""
        INSERT INTO nutricion_infantil (patient_id,edad,resultado,peso,talla,imc,dia_nutricional,or_alimentaria,observaciones)
        VALUES ('{patient_id}','{edad}','{resultado}','{peso}','{talla}','{imc}','{dia_nutricional}','{or_alimentaria}','{observaciones}')
    """.format(patient_id = info['id'],edad = info['edadmeses'],resultado = info['resultado'],peso = info['pesonino'],talla = info['tallanino'],imc = info['imcnino'],dia_nutricional = info['diagnosticonutricional'],or_alimentaria = info['orientacionalimentaria'],observaciones = info['observacionesnino'])
    try:
        cur.execute(query)
        cur.close()
        conn.close()
        return 'Escritura exitosa'
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        return response

    return 'ok'
    
    
def cards_cec(id):
    try:
        conn = psycopg2.connect(database = 'mawmedcards', user = 'mawdelta', password = 'F_t5EErS25L=zBPV', host = 'mawmedcards.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        return response
    
    query = """
        SELECT * FROM cec
        WHERE patient_id = {}
    """.format(id)

    try:
        cur.execute(query)
        card_list = []
        
        for row in cur:
            #card['id'] = int(row[1])
            card = OrderedDict()
            fecha = row[2]
            card['Fecha: '] = '{} hrs.'.format(fecha.strftime('%m/%d/%Y %H:%M'))
            card['Peso: '] = unicode(row[3],'utf-8')
            card['I.M.C.: '] = unicode(row[4],'utf-8')
            card['C.C.: '] = unicode(row[5],'utf-8')
            card[u'T.A. Sistólica: '] = unicode(row[6],'utf-8')
            card[u'T.A. Diastólica'] = unicode(row[7],'utf-8')
            card[u'Glucemia'] = unicode(row[8],'utf-8')
            card['Hba1: '] = unicode(row[9],'utf-8')
            card['Pies: '] = unicode(row[10],'utf-8')
            card['Colesterol total: '] = unicode(row[11],'utf-8')
            card['LDL: '] = unicode(row[12],'utf-8')
            card['HDL: '] = unicode(row[13],'utf-8')
            card[u'Triglicéridos: '] = unicode(row[14],'utf-8')
            card[u'Tratamiento no farmacológico: '] = unicode(row[15],'utf-8')
            card[u'Tratamiento farmacológico: '] = unicode(row[16],'utf-8')
            card['Paciente controlado: '] = unicode(row[17],'utf-8')
            card['Grupo de ayuda: '] = unicode(row[18],'utf-8')
            card['Complicaciones: '] = unicode(row[19],'utf-8')
            card['Observaciones: '] = unicode(row[20],'utf-8')
            card_list.append(card)
            print card
        return card_list
    except Exception as e:
        response = 'Error: {}'.format(e)
        return response

    cur.close()
    conn.close()

def cards_cmeyl(id):
    try:
        conn = psycopg2.connect(database = 'mawmedcards', user = 'mawdelta', password = 'F_t5EErS25L=zBPV', host = 'mawmedcards.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        return response
    
    query = """
        SELECT * FROM cmeyl
        WHERE patient_id = {}
    """.format(id)

    try:
        cur.execute(query)
        card_list = []
        
        for row in cur:
            #card['id'] = int(row[1])
            card = OrderedDict()
            fecha = row[2]
            card['Fecha: '] = '{} hrs.'.format(fecha.strftime('%m/%d/%Y %H:%M'))
            card['Gesta: '] = unicode(row[3],'utf-8')
            card[u'Cesáreas: '] = unicode(row[4],'utf-8')
            card['Abortos: '] = unicode(row[5],'utf-8')
            card['Hijos nacidos vivos: '] = unicode(row[6],'utf-8')
            card['Hijos nacidos muertos: '] = unicode(row[7],'utf-8')
            card['Anticonceptivos usados: '] = unicode(row[8],'utf-8')
            card['Tiempo de uso: '] = unicode(row[9],'utf-8')
            card[u'Hábito tabáquico: '] = unicode(row[10],'utf-8')
            card[u'Semanas de gestación: '] = unicode(row[11],'utf-8')
            card['Peso: '] = unicode(row[13],'utf-8')
            card[u'Tensión arterial: '] = unicode(row[14],'utf-8')
            card['Fondo uterino: '] = unicode(row[15],'utf-8')
            card[u'Señales de alarma: '] = unicode(row[16],'utf-8')
            card['Medicamentos: '] = unicode(row[17],'utf-8')
            card[u'Análisis clínicos: '] = unicode(row[18],'utf-8')
            card['Examen de orina: '] = unicode(row[19],'utf-8')
            card[u'Ácido fólico: '] = unicode(row[20],'utf-8')
            card['Ayuda alimentaria: '] = unicode(row[21],'utf-8')
            card['Observaciones: '] = unicode(row[22],'utf-8')
            
            card_list.append(card)
            print card
        return card_list
    except Exception as e:
        response = 'Error: {}'.format(e)
        return response
        
    cur.close()
    conn.close()

def cards_ci(id):
    try:
        conn = psycopg2.connect(database = 'mawmedcards', user = 'mawdelta', password = 'F_t5EErS25L=zBPV', host = 'mawmedcards.ccw6u6yt41ar.us-east-1.rds.amazonaws.com')
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        response = 'Ha habido un error: {}'.format(e)
        print response
        return response
    
    query = """
        SELECT * FROM nutricion_infantil
        WHERE patient_id = {}
    """.format(id)

    try:
        cur.execute(query)
        card_list = []
        
        for row in cur:
            #card['id'] = int(row[1])
            card = OrderedDict()
            fecha = row[2]
            card['Fecha: '] = '{} hrs.'.format(fecha.strftime('%m/%d/%Y %H:%M'))
            card['Edad: '] = unicode(row[3],'utf-8')
            card['Resultado: '] = unicode(row[4],'utf-8')
            card['Peso: '] = unicode(row[5],'utf-8')
            card['Talla: '] = unicode(row[6],'utf-8')
            card['I.M.C.: '] = unicode(row[7],'utf-8')
            card[u'Diagnóstico nutricional: '] = unicode(row[8],'utf-8')
            card[u'Orientación alimentaria: '] = unicode(row[9],'utf-8')
            card['Observaciones: '] = unicode(row[10],'utf-8')
            card_list.append(card)
            print card

        print card_list
        return card_list
    except Exception as e:
        response = 'Error: {}'.format(e)
        return response
        
    cur.close()
    conn.close()





