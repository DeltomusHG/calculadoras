# -*- coding: utf-8 -*-
import sys, os
sys.path.insert(0,'scripts')
from flask import Flask, render_template, session, request, flash, redirect, url_for, make_response, session 
import requests,forms
from collections import OrderedDict
import simplejson as json
import db_query
system_log = open('system_log.log','a+') 
system_log.write('----------------------------- Línea de control ----------------------------- \r\n')
app = Flask(__name__)
app.config.from_object("config")
app.secret_key = 'the-fuckin-secret-key'

############################# Login section ################################


@app.route('/', methods = ['GET','POST'])
def index():
    return render_template('index.html')

############################# Login section ################################

@app.route('/depresion_adultos')
def depresion_adultos():
    return render_template('depresion_adultos.html')

@app.route('/riesgo_cardiaco')
def riesgo_cardiaco():
    return render_template('riesgo_cardiaco.html')

@app.route('/percentil_infantes')
def percentil_infantes():
    return render_template('percentil_infantes.html')

@app.route('/imc')
def imc():
    return render_template('imc.html')

@app.route('/imc_infantes')
def imc_infantes():
    return render_template('imc_infantes.html')

@app.route('/porcentaje_grasa')
def porcentaje_grasa():
    return render_template('porcentaje_grasa.html')

@app.route('/prediabetes')
def prediabetes():
    return render_template('prediabetes.html')

@app.route('/autismo')
def autismo():
    return render_template('autismo.html')


    
@app.route('/generate_contract',methods= ['GET','POST'])
def generate_contract():
    req = json.loads(request.get_data())
    return req['class']['hash']

@app.route('/test_url', methods = ['GET','POST'])
def test_url():
    res = json.loads(request.getData())
    print res
    return 'done'


@app.route('/health_cards',methods = ['GET','POST'])
def health_cards():
    pass

@app.route('/test')
def test():
    print request.args.get('id',default = 0, type = int)
    return 'done'


@app.route('/control_infantil', methods = ['GET','POST'])
def control_infantil():
	system_log = open('system_log.log','a+') 
	if request.method == 'POST':
		try: 
			system_log.write('{}\r\n'.format(request))
			system_log.write('{}\r\n'.format(db_query.control_infantil(request.get_data())))
			system_log.write('Motherfucker starboy \r\n')
			return 'true'
		except Exception as e:
			print e
			response = 'Se ha detectado el siguiente error: {}'.format(e)
			system_log.write('{}\r\n'.format(response))
			return 'false'
		
	return render_template('test.html')



@app.route('/cec',methods = ['GET','POST'])
def cec():
	system_log = open('system_log.log','a+') 
	if request.method == 'POST':
		try: 
			system_log.write('{}\r\n'.format(request))
			system_log.write('{}\r\n'.format(db_query.cec(request.get_data())))
			print 'done'
			system_log.write('Motherfucker starboy \r\n')
			return 'true'
		except Exception as e:
			print e
			response = 'Se ha detectado el siguiente error: {}'.format(e)
			system_log.write('{}\r\n'.format(response))
			return 'false'
		
	return render_template('test.html')

@app.route('/cmeyl',methods = ['GET','POST'])
def cmeyl():
	system_log = open('system_log.log','a+') 
	if request.method == 'POST':
		try: 
			system_log.write('{}\r\n'.format(request))
			system_log.write('{}\r\n'.format(db_query.cmeyl(request.get_data())))
			print 'done'
			system_log.write('Motherfucker starboy \r\n')
			return 'true'
		except Exception as e:
			print e
			response = 'Se ha detectado el siguiente error: {}'.format(e)
			system_log.write('{}\r\n'.format(response))
			return 'false'
		
	return render_template('test.html')



@app.route('/test_db',methods = ['GET','POST'])
def test_db():
	if request.method == 'POST':
		db_query.control_infantil()
		return render_template('test.html')
	return render_template('test.html')


@app.route('/cartillas')
def cartillas():
	id = request.args.get('id', type = int)
	print id
	data_cec = db_query.cards_cec(id)
	data_cmeyl = db_query.cards_cmeyl(id)
	data_ci = db_query.cards_ci(id)
	print data_ci
	return render_template('cartillas.html',data_cec = data_cec,data_cmeyl = data_cmeyl, data_ci = data_ci)

@app.route('/search')
def search():
	return render_template('search.html')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/maw_api_documentation/fisrt_update/mXKwquH4TCb87kz6')
def maw_api():
	return render_template('maw_api_010319.html')

if __name__ == '__main__':
    app.debug = True
    app.run()
